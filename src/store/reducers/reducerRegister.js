import ACTION_REGISTER from '../actions/actionWriteRegister';
import initialState from '../initialState';

export default function change_value_register(state = initialState.value_register, action) {               
    
    switch(action.type) {
        case ACTION_REGISTER:            
             return action.value_register;                     
     default:     
             return state;
    
    }    
}