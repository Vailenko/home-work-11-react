import ACTION_USER from '../actions/actionWriteUser';
import initialState from '../initialState';

export default function change_value_user(state = initialState.value_user, action) {             
    
    switch(action.type) {
        case ACTION_USER:            
             return action.value_user;                     
     default:     
             return state;
    
    }    
}