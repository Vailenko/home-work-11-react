import { combineReducers } from 'redux';
import value_user from './reducerUser';
import value_register from './reducerRegister';

const reducers = combineReducers({
    value_user,    
    value_register
});

export default reducers;