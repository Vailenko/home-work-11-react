import ACTION_USER from '../actions/actionWriteUser';

function action_user(value) {
	return {
		type: ACTION_USER,
		value_user: value
	};
}

export default action_user;
