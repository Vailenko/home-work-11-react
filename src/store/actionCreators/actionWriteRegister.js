import ACTION_REGISTER from '../actions/actionWriteRegister';

function action_register(value) {
	return {
		type: ACTION_REGISTER,
		value_register: value
	};
}

export default action_register;
