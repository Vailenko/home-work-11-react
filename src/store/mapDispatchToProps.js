import { bindActionCreators } from 'redux';
import action_user from './actionCreators/actionWriteUser';
import action_register from './actionCreators/actionWriteRegister';

function mapDispatchToProps(component) {
	switch (component) {
		case "LoginForm": return function (dispatch) {
			return {
				change_value_user: bindActionCreators(action_user, dispatch)
			};
		};
		case "RegisterForm": return function (dispatch) {
			return {
				change_value_register: bindActionCreators(action_register, dispatch)
			};
		};
		default: return undefined;
	}
}

export default mapDispatchToProps;