function mapStateToProps(component) {
	switch (component) {
		case "LoginForm": {
			return function (state) {
				return {
					value_user: state.value_user
				};
			}
		}
		case "HomeForm": {
			return function (state) {
				return {
					value_user: state.value_user,
					value_register: state.value_register
				};
			}
		}
		case "RegisterForm": {
			return function (state) {
				return {
					value_register: state.value_register
				};
			}
		}
		default: return undefined;
	}
}

export default mapStateToProps;