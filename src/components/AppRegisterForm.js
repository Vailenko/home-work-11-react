import React from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import store from '../store/store';


class RegisterForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            txb_FirstName: this.props.value_register.firstName,
            txb_LastName: this.props.value_register.lastName,
            txb_EmailAddress: this.props.value_register.emailAddress,
            txb_BasicPassword: this.props.value_register.basicPassword,
            txb_ConfirmPassword: this.props.value_register.confirmPassword
        };

        //  this.handleInputChange = this.handleInputChange.bind(this);
    }
    handleInputChange = (event) => {
        this.setState({ [event.target.name]: event.target.value });
        //console.log('Output' + event.target.name + '/' + event.target.value);
    }
    handleAction = () => {
        let value = {
            firstName: this.state.txb_FirstName,
            lastName: this.state.txb_LastName,
            emailAddress: this.state.txb_EmailAddress,
            basicPassword: this.state.txb_BasicPassword,
            confirmPassword: this.state.txb_ConfirmPassword
        }
        console.log(value);
        this.props.change_value_register(value);
        console.log('change_value_register has been called');
        console.log('Register:' + store.getState().value_register.firstName);

    }
    render() {
        return (
            <Form className="col-sm-4 offset-sm-1">
                <h2>Register form</h2>
                <Form.Row>
                    <Col>
                        <Form.Label>First name</Form.Label>
                    </Col>
                    <Col>
                        <Form.Control name="txb_FirstName" type="text" value={this.state.txb_FirstName} placeholder="Enter first name" onChange={this.handleInputChange} />
                    </Col>
                </Form.Row>
                <Form.Row>
                    <Col>
                        <Form.Label>Lirst name</Form.Label>
                    </Col>
                    <Col>
                        <Form.Control name="txb_LastName" type="text" value={this.state.txb_LastName} placeholder="Enter last name" onChange={this.handleInputChange} />
                    </Col>

                </Form.Row>
                <Form.Row>

                    <Col>
                        <Form.Label>Email address</Form.Label>
                    </Col>
                    <Col>
                        <Form.Control name="txb_EmailAddress" type="email" value={this.state.txb_EmailAddress} placeholder="Enter email" onChange={this.handleInputChange} />
                        <Form.Text className="text-muted">
                            We'll never share your email with anyone else.
        </Form.Text>
                    </Col>

                </Form.Row>
                <Form.Row>

                    <Col>
                        <Form.Label>Password</Form.Label>
                    </Col>
                    <Col>
                        <Form.Control name="txb_BasicPassword" type="password" value={this.state.txb_BasicPassword} placeholder="Enter password" onChange={this.handleInputChange} />
                    </Col>

                </Form.Row>
                <Form.Row>

                    <Col>
                        <Form.Label>Confirm password</Form.Label>
                    </Col>
                    <Col>
                        <Form.Control name="txb_ConfirmPassword" type="password" value={this.state.txb_ConfirmPassword} placeholder="Confirm password" onChange={this.handleInputChange} />
                    </Col>

                </Form.Row>
                <br />
                <Button type="button" onClick={this.handleAction}>Register</Button>
            </Form>
        )
    }
}

export default RegisterForm;