import React from 'react';
import Form from 'react-bootstrap/Form';


class HomeForm extends React.Component {
	render() {
		return (
			<Form className="col-sm-4 offset-sm-1">    
				<h2>Home form</h2>
				{<p>Current user: {this.props.value_user.login==='' ? 'Not logged in' : this.props.value_user.login}</p>}				
			</Form>
		);
	}
}

export default HomeForm;