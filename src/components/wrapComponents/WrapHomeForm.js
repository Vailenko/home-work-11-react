import { connect } from 'react-redux';
import HomeForm from '../AppHomeForm';
import mapStateToProps from '../../store/mapStateToProps';
import mapDispatchToProps from '../../store/mapDispatchToProps';

const HomeFormWrap = connect(mapStateToProps("HomeForm"), mapDispatchToProps("HomeForm"))(HomeForm);

export default HomeFormWrap;