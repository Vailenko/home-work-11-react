import { connect } from 'react-redux';
import LoginForm from '../AppLoginForm';
import mapStateToProps from '../../store/mapStateToProps';
import mapDispatchToProps from '../../store/mapDispatchToProps';

const LoginFormWrap = connect(mapStateToProps("LoginForm"), mapDispatchToProps("LoginForm"))(LoginForm);

export default LoginFormWrap;