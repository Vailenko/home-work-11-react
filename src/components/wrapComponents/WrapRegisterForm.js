import { connect } from 'react-redux';
import RegisterForm from '../AppRegisterForm';
import mapStateToProps from '../../store/mapStateToProps';
import mapDispatchToProps from '../../store/mapDispatchToProps';

const RegisterFormWrap = connect(mapStateToProps("RegisterForm"), mapDispatchToProps("RegisterForm"))(RegisterForm);

export default RegisterFormWrap;