import React from 'react';
import PropTypes from 'prop-types';
import Form from 'react-bootstrap/Form';

const P404Form = ({title}) => {
return(
<Form className="col-sm-4 offset-sm-1">    
    <h2>{title}</h2>
</Form>
)
} 

P404Form.propTypes = {
    title: PropTypes.string,    
}

P404Form.defaultProps = {
    title: 'Default title'    
}

export default P404Form;