import React from 'react';
import store from '../store/store';
import Form from 'react-bootstrap/Form';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import { Redirect } from 'react-router-dom'


class LoginForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            login: this.props.value_user.login,
            password: this.props.value_user.password,
        };
    }
    state = {
        redirect: false
    }
    handleAction = () => {
        let value = {
            login: this.state.login,
            password: this.state.password,
        }
        this.props.change_value_user(value);
        this.setState({
            redirect: true
        })
    }
    handleLoginChange = (event) => {
        this.setState({ login: event.target.value });
    }
    handlePasswordChange = (event) => {
        this.setState({ password: event.target.value });
    }
    renderRedirect = () => {
        if (this.state.redirect) {
            return <Redirect to='/' />
        }
    }

    render() {
        return (
            <>
                <Form className="col-sm-4 offset-sm-1">
                    <h2>Login form</h2>                    
                    <Form.Row>
                        <Col>
                            <Form.Label>Login</Form.Label>
                        </Col>
                        <Col>
                            <Form.Control type="text" value={this.state.login} placeholder="login" onChange={this.handleLoginChange} />
                        </Col>
                    </Form.Row>
                    <Form.Row>
                        <Col>
                            <Form.Label>Password</Form.Label>
                        </Col>
                        <Col>
                            <Form.Control type="password" value={this.state.password} placeholder="password" onChange={this.handlePasswordChange} />
                        </Col>
                    </Form.Row>
                    <br></br>
                    {this.renderRedirect()}
                    <Button type="button" onClick={this.handleAction}>Login</Button>
                </Form>
            </>
        );
    }
}

export default LoginForm;




