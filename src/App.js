import './App.css';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
} from "react-router-dom";
import LoginFormWrap from './components/wrapComponents/WrapLoginForm';
import RegisterForm from './components/wrapComponents/WrapRegisterForm';
import P404Form from './components/App404Form';
import HomeFormWrap from './components/wrapComponents/WrapHomeForm';

function App() {
    return (
        <Router>
            <div className="App">
                <ul align="left">
                    <li>
                        <Link to="/">Home</Link>
                    </li>
                    <li>
                        <Link to="/login">Login</Link>
                    </li>
                    <li>
                        <Link to="/register">Register</Link>
                    </li>
                    <li>
                        <Link to="/p404">404</Link>
                    </li>
                </ul>

            </div>
            <Switch>
                <Route exact path="/">
                    <HomeFormWrap />
                </Route>
                <Route path="/login">
                    <LoginFormWrap />
                </Route>
                <Route path="/register">
                    <RegisterForm title="Fill out register information" />
                </Route>
                <Route path="/p404">
                    <P404Form title="Ups.. somthing went wrong... 404!" />
                </Route>
                <Route path="*">                    
                    <P404Form title="Ups.. somthing went wrong... 404!" />
                </Route>
            </Switch>
        </Router>
    );
}

export default App;
